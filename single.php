<?php get_header(); ?>
<div class="row expanded">
    <div class="column large-18">
        <?php
        if (have_posts()) {
            while (have_posts()) {
                the_post();
                ?>

                <article role="main" class="column primary-content" id="post-<?php the_ID(); ?>">
                    <header>
                        <h1><?php the_title(); ?></h1>
                    </header>

                    <?php the_post_thumbnail('full'); ?>

                    <?php the_content(); ?>

                    <?php wp_link_pages(array('before' => '<div class="page-link">' . __('Pages:'), 'after' => '</div>')); ?>

                    <footer class="entry-meta">
                        <p>Опубликовано <strong><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></strong> в <time datetime="<?php the_time('l, F jS, Y') ?>" pubdate><?php the_time('l, F jS, Y') ?></time> &middot; <a href="<?php the_permalink(); ?>">Permalink</a></p> 
                    </footer>
                </article>
                <aside class="column primary-content" >
                    <nav>
                        <ul class="navigation pagination text-center">
                            <li class="older pagination-previous">
                                <?php previous_post_link('%link', '&larr; %title'); ?>
                            </li> 
                            <li class="newer pagination-next">
                                <?php next_post_link('%link', '%title &rarr;'); ?>
                            </li>
                        </ul>
                    </nav>
                    <?php comments_template('', true); ?>
                </aside>
                <?php
            }
        } // end of the loop.  
        ?>
    </div>

    <?php get_sidebar("right"); ?>

</div>
<?php get_footer(); ?>
