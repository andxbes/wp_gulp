<?php get_header(); ?>



                    <div class="callout primary">
                        <div class="row column">
                            <h1>Hello! This is the portfolio of a very witty person.</h1>
                            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus luctus urna sed urna ultricies ac tempor dui sagittis. In condimentum facilisis porta. Sed nec diam eu diam mattis viverra. Nulla fringilla.</p>
                        </div>
                    </div>
                    <div class="row small-up-2 medium-up-3 large-up-4">
                        <div class="column">
                            <img class="thumbnail" src="https://placehold.it/550x550">
                            <h5>My Site</h5>
                        </div>
                        <div class="column">
                            <img class="thumbnail" src="https://placehold.it/550x550">
                            <h5>My Site</h5>
                        </div>
                        <div class="column">
                            <img class="thumbnail" src="https://placehold.it/550x550">
                            <h5>My Site</h5>
                        </div>
                        <div class="column">
                            <img class="thumbnail" src="https://placehold.it/550x550">
                            <h5>My Site</h5>
                        </div>
                        <div class="column">
                            <img class="thumbnail" src="https://placehold.it/550x550">
                            <h5>My Site</h5>
                        </div>
                        <div class="column">
                            <img class="thumbnail" src="https://placehold.it/550x550">
                            <h5>My Site</h5>
                        </div>
                        <div class="column">
                            <img class="thumbnail" src="https://placehold.it/550x550">
                            <h5>My Site</h5>
                        </div>
                        <div class="column">
                            <img class="thumbnail" src="https://placehold.it/550x550">
                            <h5>My Site</h5>
                        </div>
                        <div class="column">
                            <img class="thumbnail" src="https://placehold.it/550x550">
                            <h5>My Site</h5>
                        </div>
                        <div class="column">
                            <img class="thumbnail" src="https://placehold.it/550x550">
                            <h5>My Site</h5>
                        </div>
                        <div class="column">
                            <img class="thumbnail" src="https://placehold.it/550x550">
                            <h5>My Site</h5>
                        </div>
                        <div class="column">
                            <img class="thumbnail" src="https://placehold.it/550x550">
                            <h5>My Site</h5>
                        </div>
                    </div>

<?php
	if ( have_posts() ){
		the_post();
        }
?>

<h1>
	<?php if ( is_day() ) : ?>
        <?php printf( __( 'Daily Archives: <span>%s</span>' ), get_the_date() ); ?>
    <?php elseif ( is_month() ) : ?>
        <?php printf( __( 'Monthly Archives: <span>%s</span>' ), get_the_date('F Y') ); ?>
    <?php elseif ( is_year() ) : ?>
        <?php printf( __( 'Yearly Archives: <span>%s</span>' ), get_the_date('Y') ); ?>
    <?php else : ?>
        <?php _e( 'The Blog' ); ?>
    <?php endif; ?>
</h1>

<?php
	/* Since we called the_post() above, we need to
	 * rewind the loop back to the beginning that way
	 * we can run the loop properly, in full.
	 */
	rewind_posts();

	/* Run the loop for the archives page to output the posts.
	 * If you want to overload this in a child theme then include a file
	 * called loop-archives.php and that will be used instead.
	 */
	 get_template_part( 'loop', 'archive' );
?>

<?php get_footer(); ?>
