<aside class="off-canvas position-left reveal-for-large" id="sidebar-left" data-off-canvas="" data-position="left" aria-hidden="true">
    <div class="row column">
        <img class="thumbnail" alt="logo" src="https://placehold.it/550x350">
        <h5><?= bloginfo('name'); ?></h5>
        <p><?= bloginfo('description'); ?></p>
    </div>
    <div class="row column">
        <?php
        add_filter("nav_menu_submenu_css_class", function($classes, $args, $depth ) {

            $classes[] = "vertical";

            $classes[] = "nested";


            return $classes;
        }, 10, 3);
        ?>
        <?php
        wp_nav_menu(array(
            'theme_location' => 'primary',
            'menu' => '',
            'container' => 'nav',
            'container_class' => '',
            'container_id' => '',
            'menu_class' => ' vertical menu drilldown',
            'menu_id' => 'left-primary-navigation',
            'echo' => true,
            'fallback_cb' => 'wp_page_menu',
            'before' => '',
            'after' => '',
            'link_before' => '',
            'link_after' => '',
            'items_wrap' => '<ul id="%1$s" class="%2$s" data-drilldown data-animate-height="true" data-close-on-click="true" data-auto-apply-class="true" >%3$s</ul>',
            'depth' => 0,
            'walker' => new PrimaryMenu(),
        ))
        ?>

    </div>
    <div class="row column">
        <?php if (function_exists('dynamic_sidebar') & is_active_sidebar('sidebar-2')) { ?>
            <?php dynamic_sidebar("sidebar-2") ?>
        <?php } ?>
    </div>
</aside>

