<!DOCTYPE html>
<html <?php language_attributes(); ?>> 
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <?php wp_head(); ?>

    </head>

    <body <?php body_class(); ?> id="top">

        <div class="off-canvas-wrapper">
            <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper="">
                <?php get_sidebar("left")?>
                <div class="off-canvas-content" data-off-canvas-content="">
                    <div class="title-bar hide-for-large">
                        
                        <div class="title-bar-left">
                            <button class="menu-icon" type="button" data-open="sidebar-left" aria-expanded="false" aria-controls="sidebar-left"></button>
                            <span class="title-bar-title"><?=	bloginfo('name');?></span>
                        </div>
<!--                        
                        right-sidebar
                        <div class="title-bar-right">
                            <button class="menu-icon" type="button" data-open="sidebar-right" aria-expanded="false" aria-controls="sidebar-right"></button>
                            <span class="title-bar-title">Меню</span>
                        </div>
                        -->
                    </div>
                    <div class="top-bar"> <div class="row expanded columns"> 
                            <div class="top-bar-left"> 
                                <nav> 
                                    <?php
                                    if (function_exists('yoast_breadcrumb')) {
                                        yoast_breadcrumb('<div id="breadcrumbs" class="breadcrumbs">', '</div>');
                                    }
                                    ?>
                                </nav> 
                            </div> 
                            <div class="top-bar-right">
                               :)
                            </div>
                        </div> 
                    </div>



                    
 



 
