                    

                   
                    <footer class="row expanded footer" role="contentinfo">
                                            <hr>
                                            
                        <div class="medium-12 footer-form columns">
                            <h3>Contact Me</h3>
                            <p>Vivamus hendrerit arcu sed erat molestie vehicula. Sed auctor neque eu tellus rhoncus ut eleifend nibh porttitor. Ut in nulla enim. Phasellus molestie magna non est bibendum non venenatis nisl tempor. Suspendisse dictum feugiat nisl ut dapibus. Mauris iaculis porttitor.</p>
                            <ul class="menu">
                                <li><a href="#">Dribbble</a></li>
                                <li><a href="#">Facebook</a></li>
                                <li><a href="#">Yo</a></li>
                            </ul>
                        </div>
                        <div class="medium-12  columns">
                            <div class="row">
                                <?= do_shortcode('[contact-form-7 id="708" title="FooterForm"]')?>
                            </div>
                            
                        </div>
                 
                    <div class="js-off-canvas-exit"></div>  
                        
                        
                        
                        <div class="large-24 column">
                        <p>&copy;<?php echo date("Y"); ?> <a href="#top" title="Jump back to top">&#8593;</a></p>
                        </div>
                    </footer>
                </div>
                <!--/off-canvas-content-->
                
            </div>
        </div>
        <?php wp_footer(); ?>
    </body>
</html>
