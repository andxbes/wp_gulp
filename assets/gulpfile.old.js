/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const source = "./source";
const dist = "./dist";

//libs
const gulp = require('gulp'),
        sass = require('gulp-sass'),
//        browserSync = require('browser-sync'), // Подключаем Browser Sync
        concat = require('gulp-concat'), // Подключаем gulp-concat (для конкатенации файлов)
        uglify = require('gulp-uglify'); // Подключаем gulp-uglifyjs (для сжатия JS)
const  minifyCss = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const include = require('gulp-include');
//const sourcemaps = require("gulp-sourcemaps");





gulp.task('scss', function () { // Создаем таск "sass"
    return gulp.src(source + '/scss/**/*.+(scss|sass)') // Берем источник
            .pipe(sass().on('error', sass.logError))
//            .pipe(sourcemaps.init())
            .pipe(autoprefixer())
            //.pipe(concat('all.css'))// если нужно дать свое имя выходному файлу , отличному от главного . 
//            .pipe(sourcemaps.write('.'))
            .pipe(minifyCss({

                keepSpecialComments: 1,

            })) // Преобразуем Sass в CSS посредством gulp-sass
            .pipe(gulp.dest(dist + '/css')); // Выгружаем результата в папку app/css
});

//
gulp.task('browser-sync', function () { // Создаем таск browser-sync
    browserSync({// Выполняем browser Sync
        server: {// Определяем параметры сервера
            baseDir: '../../../' // Директория для сервера - app
        },
        notify: false // Отключаем уведомления
    });
});



gulp.task('scripts', function () {
//    var gutil = require('gulp-util');
//
//$.uglify().on('error', function(err) {
//gutil.log(gutil.colors.red('[Error]'), err.toString());
//this.emit('end');
//})



    return gulp.src(// Берем все необходимые библиотеки
            source + '/js/*.+(js)') //Подключаем все Js  файлы из папки 
            .pipe(include())
            .pipe(concat('js.min.js')) // Собираем их в кучу в новом файле libs.min.js
            .pipe(uglify()) // Сжимаем JS файл
            .pipe(gulp.dest(dist + '/js')); // Выгружаем в папку app/js
});


gulp.task('default', ["scripts","scss",'browser-sync'], function () {
    gulp.watch(source + '/scss/**/*.+(scss|sass)', ['scss']); // Наблюдение за sass
    gulp.watch(source + '/js/**/*.+(js)', ['scripts']); // Наблюдение за sass
});
