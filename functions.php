<?php

function theme_setup() {
    
    load_theme_textdomain('_theme', get_template_directory() . '/languages');
    
    add_theme_support('menus');
    add_theme_support('title-tag');

    add_theme_support('post-thumbnails');
    add_image_size("mini_manager", 80, 67);

   
    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support('html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ));

    /*
     * Enable support for Post Formats.
     * See https://developer.wordpress.org/themes/functionality/post-formats/
     */
    add_theme_support('post-formats', array(
        'aside',
        'image',
        'video',
        'quote',
        'link',
    ));


    // Set up the WordPress core custom background feature.
    add_theme_support('custom-background', apply_filters('medic_theme_custom_background_args', array(
        'default-color' => 'ffffff',
        'default-image' => '',
    )));


    add_post_type_support('page', 'excerpt');
}

add_action('after_setup_theme', 'theme_setup');

function theme_widgets_init() {
    register_sidebar(array(
        'name' => "В правом сайдбаре",
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'medic_theme'),
        'before_widget' => '<section id="%1$s" class="widget side_widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<header class="widget-title h5 cat_title">',
        'after_title' => '</header>',
    ));

//	wcm

    register_sidebar(array(
        'name' => "В левом сайдбаре , под меню ",
        'id' => 'sidebar-2',
        'description' => esc_html__('Add widgets here.', 'medic_theme'),
        'before_widget' => '<section id="%1$s" class="widget sidebar2 under_content %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<header class="widget-title h5 cat_title">',
        'after_title' => '</header>',
    ));

    register_sidebar(array(
        'name' => "В шапке ",
        'id' => 'sidebar-3',
        'description' => esc_html__('Add widgets here.', 'medic_theme'),
        'before_widget' => '<section id="%1$s" class="widget header_widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<header class="widget-title h5 cat_title">',
        'after_title' => '</header>',
    ));
}

add_action('widgets_init', 'theme_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function theme_scripts() {


    //wp_register_script('foundation', get_template_directory_uri() . '/assets/dist/js/foundation.min.js', array("jquery"), '13-09-2017', true);
    wp_enqueue_script('_jsLib', get_template_directory_uri() . '/assets/dist/js/lib.js', array("jquery"), '13-09-2017', true);
    wp_enqueue_script('_js', get_template_directory_uri() . '/assets/dist/js/functions.js', array("jquery"), '13-09-2017', true);


//    if (is_singular() && comments_open() && get_option('thread_comments')) {
//        wp_enqueue_script('comment-reply');
//    }

//    wp_register_script('socialLib', '//yastatic.net/es5-shims/0.0.2/es5-shims.min.js', array(), '13-09-2017', true);
//    wp_register_script('socialShare', '//yastatic.net/share2/share.js', array('socialLib'), '13-09-2017', true);
}

add_action('wp_enqueue_scripts', 'theme_scripts');

function theme_styles() {
//    wp_enqueue_style('_foundation', get_template_directory_uri() . "/assets/dist/css/foundation-float.css", array(), "13-09-2017");
    wp_enqueue_style('_theme-style', get_template_directory_uri() . "/assets/dist/css/style.css", array(), "13-09-2017");
    wp_enqueue_style('_s-users-style', get_stylesheet_uri());
}

add_action('wp_print_styles', 'theme_styles');

/**
 * Registers an editor stylesheet for the theme.
 */
function wpdocs_theme_add_editor_styles() {
    add_editor_style(get_template_directory_uri() . '/assets/dist/css/main.css');
}

add_action('admin_init', 'wpdocs_theme_add_editor_styles');



// Custom functions 
// Tidy up the <head> a little. Full reference of things you can show/remove is here: http://rjpargeter.com/2009/09/removing-wordpress-wp_head-elements/
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'wp_generator'); //убираем версию wp
remove_action('wp_head', 'rel_canonical');


/* удаляем последний элемент в хлебных крошках */

//<span class="breadcrumb_last">Наши услуги</span></span>

add_filter('wpseo_breadcrumb_single_link', function ($link_output) {
    if (strpos($link_output, 'breadcrumb_last') == true) {
        $reg = "/<span.*?class=['\"].*?['\"].*?>(.*?)<\/span>/is";
        $result = null;
        preg_match($reg, $link_output, $result);
        if (isset($result[0])) {
            $link_output = str_replace($result[0], "", $link_output);
        }
    }
    return $link_output;
});

//add_filter('wpseo_breadcrumb_single_link',function($link){
//    
//    
//    
////    return "<li>".$link."</li>";
//});


//add_filter( 'wpseo_breadcrumb_single_link', 'bybe_crumb_fix' , 10, 2 );
//function bybe_crumb_fix( $output, $crumb ){
//  if ( is_array( $crumb ) && $crumb !== array() ) {               
//    if( strpos( $output, '<span class="breadcrumb_last"' ) !== false  ||   strpos( $output, '<strong class="breadcrumb_last"' ) !== false ) { 
//      $output = '<a property="v:title" rel="v:url" href="'. $crumb['url']. '" >';
//      $output.= $crumb['text'];
//      $output.= '</a>';
//    } else { $output .= "</span>"; }
//  }
//  return $output;
//}

include 'inc/menues.php';
